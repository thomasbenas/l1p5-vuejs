#!/bin/bash

# Start labo1.5 website

# @author  Gerald Salin
# @author  Fabrice Jammes

set -euo pipefail

DIR=$(cd "$(dirname "$0")"; pwd -P)

usage() {
    cat << EOD
Usage: $(basename "$0") [options]
Available options:
  -h            This message

Start labo1.5 website

EOD
}

while getopts h c ; do
    case $c in
        h) usage ; exit 0 ;;
        \?) usage ; exit 2 ;;
    esac
done
shift "$((OPTIND-1))"

if [ $# -ne 0 ] ; then
    usage
    exit 2
fi

echo "Start l1p5 website"

echo "Start Apache"
apache2ctl -D FOREGROUND
