/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import { createExportFile } from '@/utils/files'

export const commute = {
  methods: {
    exportCommutes: function (GHGIyear) {
      this.$store.dispatch('ghgi/compute')
      let results = this.ghgi.emissions.commutes
      let csvContent = [
        ['seqid', 'commuting.days', 'position', 'engine.std1', 'motorbikepooling.std1', 'carpooling.std1',
          'walking.std1.km', 'bike.std1.km', 'ebike.std1.km', 'escooter.std1.km',
          '2wheeler.std1.km', 'car.std1.km', 'bus.std1.km', 'busintercity.std1.km',
          'tramway.std1.km', 'train.std1.km',
          'rer.std1.km', 'subway.std1.km',
          'commuting.std2.days', 'engine.std2', 'motorbikepooling.std2', 'carpooling.std2',
          'walking.std2.km', 'bike.std2.km', 'ebike.std2.km', 'escooter.std2.km',
          '2wheeler.std2.km', 'car.std2.km', 'bus.std2.km', 'busintercity.std2.km',
          'tramway.std2.km', 'train.std2.km',
          'rer.std2.km', 'subway.std2.km',
          'walking.kg.co2e', 'bike.kg.co2e', 'ebike.kg.co2e', 'escooter.kg.co2e',
          '2wheeler.kg.co2e', 'car.kg.co2e', 'bus.kg.co2e', 'busintercity.kg.co2e',
          'tramway.kg.co2e', 'train.kg.co2e',
          'rer.kg.co2e', 'subway.kg.co2e',
          'total.kg.co2e'].join('\t'),
        ...results.totalPerCommute.map(function (item, index) {
          let toReturn = results.meta[index].seqID + '\t' + results.meta[index].nWorkingDay + '\t'
          toReturn += results.meta[index].position + '\t' + results.meta[index].engine + '\t'
          toReturn += results.meta[index].motorbikepooling + '\t' + results.meta[index].carpooling + '\t'
          toReturn += results.rawDistance1[index].map(val => Math.round(val)).join('\t')
          toReturn += '\t' + results.meta[index].nWorkingDay2 + '\t' + results.meta[index].engine2 + '\t'
          toReturn += results.meta[index].motorbikepooling2 + '\t' + results.meta[index].carpooling2 + '\t'
          toReturn += results.rawDistance2[index].map(val => Math.round(val)).join('\t')
          toReturn += '\t' + item.slice(1, item.length).map(obj => Math.round(obj.intensity)).join('\t')
          toReturn += '\t' + Math.round(item.slice(1, item.length).map(obj => obj.intensity).reduce(function (a, b) {
            return a + b
          }, 0))
          return toReturn
        })
      ]
        .join('\n')
        .replace(/(^\[)|(\]$)/gm, '')
      createExportFile(csvContent, 'ghgi' + GHGIyear + '_commutes')
    }
  }
}
