import * as Papa from 'papaparse'

// Search for code nacre and amount columns
// within header, and return the matches as
// {icode,iamount}
function _parseHeaders (header) {
  function _findHeader (regex) {
    for (let name of header) {
      if (name.search(regex) >= 0) {
        return name
      }
    }
    return null
  }
  return {
    icode: _findHeader(/(code|nacre|code.nacre|codenacre|nacrecode)/i),
    iamount: _findHeader(/(montant|amount|somme|total|€)/i)
  }
}

const parseFile = (file) => new Promise((resolve, reject) => {
  Papa.parse(file, {
    header: true,
    worker: true,
    delimitersToGuess: [',', '\t', ';'],
    skipEmptyLines: true,
    complete: function (result) {
      let headerMap = _parseHeaders(result.meta.fields)
      if (result.errors.length > 0) {
        resolve([result.data, headerMap, result.errors])
      } else {
        resolve([result.data, headerMap, 'ok'])
      }
    },
    error: function (error) {
      reject(error)
    }
  })
})

export { parseFile }
