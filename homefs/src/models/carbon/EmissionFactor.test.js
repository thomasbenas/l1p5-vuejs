import EmissionFactor from './EmissionFactor'

/**
 *
 * @param {Object} modifier key, value to modify in the default EmissionFactor Object
 *
 * @returns {EmissionFactor}
 */
function emissionFactorObj (modifier) {
  let ef = {
    'decomposition': {
      '2018': {
        'total': {
          'total': 0.2234,
          'uncertainty': 0.6
        },
        'combustion': {
          'total': 0.162,
          'uncertainty': 0.6
        },
        'upstream': {
          'total': 0.0358,
          'uncertainty': 0.6
        },
        'manufacturing': {
          'total': 0.0256,
          'uncertainty': 0.6
        }
      }
    }
  }

  if (modifier === undefined) {
    modifier = {}
  }

  for (let [key, value] of Object.entries(modifier)) {
    ef[key] = value
  }
  return EmissionFactor.createFromObj(ef)
}

describe('EmissionFactor', () => {
  test('creator with default values', () => {
    let ef = EmissionFactor.createFromObj()
    expect(ef.hasDecomposition()).toBe(true)
    expect(ef.getFactor().total.total).toBe(0)
    expect(ef.getFactor().combustion.co2).toBe(0)
    expect(ef.getFactor().upstream.uncertainty).toBe(0)
    expect(ef.getFactor().manufacturing.others).toBe(0)
  })

  test('creator with 2018 car emissions', () => {
    let ef = emissionFactorObj()
    expect(ef.hasDecomposition()).toBe(true)
    expect(ef.getYear()).toBe('2018')
    expect(ef.getYear('2020')).toBe('2018')
    expect(ef.getDecompositionKeys().length).toBe(4)
    expect(ef.getDecompositionKeys()[0]).toBe('total')
    expect(ef.getDecompositionKeys()[2]).toBe('upstream')
    expect(ef.getFactor().total.total).toBe(0.2234)
    expect(ef.getFactor().combustion.co2).toBe(0)
    expect(ef.getFactor().upstream.uncertainty).toBe(0.6)
    expect(ef.getFactor().manufacturing.others).toBe(0)
  })
})
