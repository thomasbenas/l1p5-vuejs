/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import { CarbonIntensity, CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'
import PURCHASES_FACTORS from '@/../data/purchasesFactors.json'
import Modules from '@/models/Modules.js'
import EmissionFactor from '@/models/carbon/EmissionFactor.js'

const TRAVELLING_EXPENSES_CODES = ['xa01', 'xa02', 'xa11', 'xa12']
const WARNING_RATE = 20

export default class Purchase {
  constructor (id, code, amount, source = null) {
    this.id = id
    this.setCode(code)
    this.amount = this.setAmount(amount)
    this.source = source
    this.module = this.setModule()
  }

  get score () {
    let score = 1
    if (this.module !== Purchase.MODULE_NAME) {
      score = 0
    } else if (!Object.keys(PURCHASES_FACTORS).includes(this.code) && isNaN(this.amount)) {
      score = 2
    }
    return score
  }

  isOther () {
    return this.score === 0
  }

  isValid () {
    return this.score === 1
  }

  isIncomplete () {
    return false
  }

  isInvalid () {
    return this.score === 2
  }

  setCode (value) {
    let lvalue = value.toLowerCase().replace('.', '').trim()
    this.code = lvalue
  }

  setAmount (value) {
    let amount = value
    if (typeof amount === 'string') {
      amount = amount.replace(',', '.').trim().replace(/\s/g, '')
    }
    if (!isNaN(amount)) {
      amount = parseFloat(amount)
    }
    return amount
  }

  setModule () {
    return this.getFactorModule()
  }

  getCarbonIntensity (GHGIYear) {
    let ef = this.getEmissionFactor(this.code, GHGIYear)
    return new CarbonIntensity(
      this.amount * ef.total.total,
      this.amount * ef.total.total * ef.total.uncertainty,
      null,
      null,
      ef.group
    )
  }

  getEmissionFactor (code, year) {
    let ef = EmissionFactor.createFromObj(PURCHASES_FACTORS[code])
    return ef.getFactor(year)
  }

  getFactorDescription () {
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      return PURCHASES_FACTORS[this.code].description_FR
    } else {
      return '-'
    }
  }

  getFactorModule () {
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      return PURCHASES_FACTORS[this.code].module
    } else {
      return null
    }
  }

  getCatGHGP () {
    let catGHGP = '3.1'
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      if (PURCHASES_FACTORS[this.code].catGHGP !== null) {
        catGHGP = PURCHASES_FACTORS[this.code].catGHGP
      }
    }
    return catGHGP
  }

  getCatV5 () {
    let catV5 = '4.1'
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      if (PURCHASES_FACTORS[this.code].catV5 !== null) {
        catV5 = PURCHASES_FACTORS[this.code].catV5
      }
    }
    return catV5
  }

  getCategory () {
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      return PURCHASES_FACTORS[this.code].category
    } else {
      return null
    }
  }

  getDescription () {
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      return PURCHASES_FACTORS[this.code].description_FR
    } else {
      return null
    }
  }

  getCodeFromDescription (value) {
    let lvalue = value.toLowerCase()
    let fcode = null
    for (let code of Object.keys(PURCHASES_FACTORS)) {
      if (PURCHASES_FACTORS[code].description_FR.toLowerCase() === lvalue) {
        fcode = code
      }
    }
    return fcode
  }

  toDatabase () {
    return {
      'code': this.code,
      'amount': this.amount
    }
  }

  static isACode (code) {
    let regex = new RegExp('^[a-z]{2}[0-9]{2}$')
    return regex.test(code.toLowerCase())
  }

  static reduce (purchases) {
    let reducedPurchases = []
    for (let purchase of purchases) {
      let index = reducedPurchases.findIndex(obj => obj.code === purchase.code)
      if (index < 0) {
        reducedPurchases.push(purchase)
      } else {
        reducedPurchases[index].amount += purchase.amount
      }
    }
    return reducedPurchases
  }

  static getTravellingExpensesCodes () {
    return TRAVELLING_EXPENSES_CODES
  }

  static getWarningRate () {
    return WARNING_RATE
  }

  static getCatGHGP () {
    let catGHGP = Object.keys(PURCHASES_FACTORS).map(function (code) {
      return PURCHASES_FACTORS[code].catGHGP
    })
    return Array.from(new Set(catGHGP)).filter(val => val !== null)
  }

  static getCatV5 () {
    let catV5 = Object.keys(PURCHASES_FACTORS).map(function (code) {
      return PURCHASES_FACTORS[code].catV5
    })
    return Array.from(new Set(catV5)).filter(val => val !== null)
  }

  static getCategories () {
    let categories = Object.keys(PURCHASES_FACTORS).map(function (code) {
      return PURCHASES_FACTORS[code].category
    })
    return Array.from(new Set(categories)).filter(val => val !== null)
  }

  static getCodes () {
    let codes = Object.keys(PURCHASES_FACTORS)
    return Array.from(new Set(codes))
  }

  static getDescriptions () {
    let descriptions = Object.keys(PURCHASES_FACTORS).map(function (code) {
      return PURCHASES_FACTORS[code].description_FR
    })
    return Array.from(new Set(descriptions))
  }

  static getDescription (code) {
    if (Object.keys(PURCHASES_FACTORS).includes(code)) {
      return PURCHASES_FACTORS[code].description_FR
    } else {
      return null
    }
  }

  static get MODULE_NAME () {
    return Modules.PURCHASES
  }

  static createFromObj (item) {
    return new Purchase(
      item.id,
      item.code,
      item.amount,
      item.source
    )
  }

  static compute (purchases, GHGIYear) {
    let intensities = new CarbonIntensities()
    let meta = []
    let catGHGP = {}
    let catV5 = {}
    let categories = {}
    let totalXA = 0
    for (let purchase of purchases) {
      let intensity = purchase.getCarbonIntensity(GHGIYear)
      let ccatGHGP = purchase.getCatGHGP()
      let ccatV5 = purchase.getCatV5()
      let category = purchase.getCategory()
      meta.unshift({
        'code': purchase.code,
        'catGHGP': ccatGHGP,
        'catV5': ccatV5,
        'category': category,
        'module': purchase.getFactorModule(),
        'description': purchase.getDescription(),
        'amount': purchase.amount,
        'intensity': intensity
      })
      if (purchase.getFactorModule() === Purchase.MODULE_NAME) {
        intensities.add(intensity)
        if (Object.keys(catGHGP).includes(ccatGHGP.toString())) {
          catGHGP[ccatGHGP.toString()].add(intensity)
        } else {
          catGHGP[ccatGHGP.toString()] = new CarbonIntensities()
          catGHGP[ccatGHGP.toString()].add(intensity)
        }
        if (Object.keys(catV5).includes(ccatV5.toString())) {
          catV5[ccatV5.toString()].add(intensity)
        } else {
          catV5[ccatV5.toString()] = new CarbonIntensities()
          catV5[ccatV5.toString()].add(intensity)
        }
        if (Object.keys(categories).includes(category)) {
          categories[category].add(intensity)
        } else {
          categories[category] = new CarbonIntensities()
          categories[category].add(intensity)
        }
        if (TRAVELLING_EXPENSES_CODES.includes(purchase.code)) {
          totalXA += intensity.intensity
        }
      }
    }
    Object.keys(categories).map(function (key) {
      categories[key] = categories[key].sum()
    })
    Object.keys(catGHGP).map(function (key) {
      catGHGP[key] = catGHGP[key].sum()
    })
    Object.keys(catV5).map(function (key) {
      catV5[key] = catV5[key].sum()
    })
    return {
      'intensity': intensities.sum(),
      'meta': meta,
      'catGHGP': catGHGP,
      'catV5': catV5,
      'categories': categories,
      'totalXA': totalXA
    }
  }
}
