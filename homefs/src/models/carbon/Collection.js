import Travel from '@/models/carbon/Travel.js'
import TravelSection from '@/models/carbon/TravelSection.js'
import Building from '@/models/carbon/Building.js'
import Vehicle from '@/models/carbon/Vehicle.js'
import ComputerDevice from '@/models/carbon/ComputerDevice.js'
import Purchase from '@/models/carbon/Purchase.js'
import Commute from '@/models/carbon/Commute.js'

/**
 * NotImplemented error class for Abstract methods
 */
class NotImplemented extends Error {
  constructor (message = '', ...args) {
    super(message, ...args)
  }
}

/**
 * Manage a list of items
 * We keep an index up-to-date for efficient lookup
 */
export class Collection {
  constructor () {
    // This will store the mapping hash(t) -> index
    this.indexes = new Map()
    // This will store the actual objects
    this.items = []
  }

  /**
   * Get the number of objects
   */
  get length () {
    return this.items.length
  }

  /**
   * Hash function. Has to be implemented by sub-classes.
   * Returns string that considere two items are the same.
   * @param {*} item
   * @returns
   */
  hash (item) {
    throw new NotImplemented('Abstract method hash has not been implemented')
  }

  /**
   * Count the number of item returned by the testFunction
   * @param {*} testFunction
   * @param {*} param
   * @returns
   */
  countItems (testFunction, param) {
    let nb = 0
    for (let item of this.items) {
      if (testFunction(item)) {
        nb++
      }
    }
    return nb
  }

  sortItems () {
    // pass
  }

  /**
   * Add an item to the collection
   * The hash function is used to know whether we already have the travel recorded.
   * @param {*} item
   */
  add (item) {
    this.items.push(item)
    this.indexes.set(this.hash(item), this.items.length - 1)
  }

  /**
   * Update an item of the collection
   * The hash function is used to know which item to update using itemfrom
   * @param {*} { itemfrom , itemto }
   */
  update (iobj) {
    let h = this.hash(iobj.from)
    let index = this.indexes.get(h)
    if (index === undefined) {
      return
    }
    Object.assign(this.items[index], iobj.to)
  }

  /**
   * Reduce the item lists
   */
  reduceItems () {
    return this.items
  }

  /**
   * Some operations require to re-create the index.
   * For instance when we delete an travel the index will get out-of-sync
   * (because posterior index needs to be decremented by 1)
   *
   * O(n) complexity
   */
  reIndex () {
    this.indexes = new Map()
    for (let index in this.items) {
      let h = this.hash(this.items[index])
      this.indexes.set(h, index)
    }
  }

  _delete (cb) {
    let toRemove = []
    for (let t of this.items) {
      if (cb(t)) {
        // update the indexes
        let h = this.hash(t)
        let index = this.indexes[h]
        toRemove.push(index)
      }
    }

    for (let index of toRemove) {
      this.items.splice(index, 1)
    }

    this.reIndex()
  }

  delete (travel) {
    let h = this.hash(travel)
    let index = this.indexes.get(h)
    if (index === undefined) {
      return
    }
    this.items.splice(index, 1)
    this.reIndex()
  }

  reset () {
    this.indexes = new Map()
    this.items = []
  }

  /**
   * Delete all the items for a given source
   * @param {*} source
   */
  deleteSource (source) {
    this._delete((t) => t.source === source)
  }
  /**
   * Delete all items that are invalid
   */
  deleteInvalid () {
    this._delete((t) => t.isInvalid())
  }

  /**
   *
   * Find if an item already exists into the collection
   *
   * O(1) complexity.
   * @param {*} travel
   * @returns
   */
  lookup (travel) {
    // get the index based on the hash result
    let index = this.indexes.get(this.hash(travel))
    // beware that 0 is a valid index
    if (index !== undefined) {
      return this.items[index]
    }
    return null
  }

  /**
   * Allows to iterate natively on a collection
   * @returns
   */
  [Symbol.iterator] () {
    return this.items.values()
  }

  /**
   * Use it where vue and our legacy code expect an array
   * @returns
   */
  toArray () {
    return this.items
  }

  filter (cb) {
    return this.items.filter(cb)
  }

  find (cb) {
    return this.items.find(cb)
  }

  map (cb) {
    return this.items.map(cb)
  }
}

/**
 * Manage a list of building
 */
export class BuildingCollection extends Collection {
  /**
   * Hash function. Two buildings with the same
   *
   * @param {*} item
   * @returns
   */
  hash (item) {
    if (item.id !== null) {
      return item.id
    } else {
      return item.name
    }
  }

  add (item) {
    let nitem = Building.createFromObj(item)
    this.items.push(nitem)
    this.indexes.set(this.hash(nitem), this.items.length - 1)
  }

  update (iobj) {
    let h = this.hash(iobj.from)
    let index = this.indexes.get(h)
    if (index === undefined) {
      return
    }
    Object.assign(this.items[index], Building.createFromObj(iobj.to))
  }

  deleteSource (source) {
    // Delete buildings imported from source
    this._delete((t) => t.source === source)
    // Delete refrigerants imported from source
    for (let item of this.items) {
      if (item.sourceRefrigerant === source) {
        item.refrigerants = []
      }
    }
  }
}

/**
 * Manage a list of device
 */
export class DeviceCollection extends Collection {
  /**
   * Hash function. Two devices with the same
   *
   * @param {*} item
   * @returns
   */
  hash (item) {
    if (item.ecodiagObject) {
      return item.ecodiagObject.id
    } else {
      return item.id
    }
  }

  add (item) {
    let nitem
    // Check if item is an ecodiag object (e.g. has a field 'details').
    // if so use fromEcodiag() otherwise create a ComputerDevice object
    // with an empty ecodiagObject field
    if (item.details) {
      nitem = ComputerDevice.fromEcodiag(item)
    } else {
      nitem = ComputerDevice.createFromObj(item)
    }
    this.items.push(nitem)
    this.indexes.set(this.hash(nitem), this.items.length - 1)
  }

  /**
   * Device update handle add and update due to ecodiag
   *
   * @param {*} item
   * @returns
   */
  update (item) {
    let h = this.hash(item)
    let index = this.indexes.get(h)
    if (index === undefined) {
      this.add(item)
    } else {
      Object.assign(this.items[index], ComputerDevice.fromEcodiag(item))
    }
  }
}

/**
 * Manage a list of commute
 */
export class CommuteCollection extends Collection {
  /**
   * Hash function. Two commutes with the same seqID
   *
   * @param {*} item
   * @returns
   */
  hash (item) {
    return item.seqID
  }

  add (item) {
    let nitem = Commute.createFromObj(item)
    this.items.push(nitem)
    this.indexes.set(this.hash(nitem), this.items.length - 1)
  }

  update (iobj) {
    let h = this.hash(iobj.from)
    let index = this.indexes.get(h)
    if (index === undefined) {
      return
    }
    Object.assign(this.items[index], Commute.createFromObj(iobj.to))
  }
}

/**
 * Manage a list of purchases
 */
export class PurchaseCollection extends Collection {
  /**
   * Hash function. Two purchase with the same code are considered the same
   *
   * @param {*} item
   * @returns
   */
  hash (item) {
    if (item.id !== null) {
      return item.id
    } else {
      return item.code
    }
  }

  add (item) {
    let nitem = Purchase.createFromObj(item)
    this.items.push(nitem)
    this.indexes.set(this.hash(nitem), this.items.length - 1)
  }

  update (iobj) {
    let h = this.hash(iobj.from)
    let index = this.indexes.get(h)
    if (index === undefined) {
      return
    }
    Object.assign(this.items[index], Purchase.createFromObj(iobj.to))
  }

  reduceItems () {
    return Purchase.reduce(this.items)
  }
}

/**
 * Manage a list of vehicles
 */
export class VehicleCollection extends Collection {
  /**
   * Hash function. Two vehicles with the same id or name are considered the same
   *
   * @param {*} item
   * @returns
   */
  hash (item) {
    if (item.id !== null) {
      return item.id
    } else {
      return item.name
    }
  }

  add (item) {
    let nitem = Vehicle.createFromObj(item)
    this.items.push(nitem)
    this.indexes.set(this.hash(nitem), this.items.length - 1)
  }

  update (iobj) {
    let h = this.hash(iobj.from)
    let index = this.indexes.get(h)
    if (index === undefined) {
      return
    }
    Object.assign(this.items[index], Vehicle.createFromObj(iobj.to))
  }
}

/**
 * Manage a list of travels
 *
 * We keep an index up-to-date for efficient lookup
 *
 */
export class TravelCollection extends Collection {
  /**
   * Hash function. Two travels with the same hash are considered as the same
   * travel
   *
   * @param {*} travel
   * @returns
   */
  hash (travel) {
    return travel.names.join('-') + '_' + travel.amount
  }

  /**
   * Add a travel to the collection
   *
   * - travel correspond to one line in a uploaded file
   * in this case we lookup if it correspond to a previously added one.
   * in this case only the section are added to the lookep-up travel
   * - travel might correspond to full travel (with all the sections) coming
   * from the user form. In this case we also look up before.
   *
   * The hash function is used to know whether we already have the travel recorded.
   *
   * @param {*} travel
   */
  add (travel, needSort = false) {
    let t = this.lookup(travel)
    if (!t) {
      // travel isn't in the collection let's add one
      t = new Travel(
        travel.names,
        travel.date,
        [],
        travel.amount,
        travel.purpose,
        travel.status,
        travel.source
      )
      this.items.push(t)
      this.indexes.set(this.hash(t), this.items.length - 1)
    }

    for (let section of travel.sections) {
      t.addSection(TravelSection.createFromObj(section))
    }

    if (needSort && t.sections.length > 0) {
      t.sortSections()
    }
  }

  /**
   * Sort the section for all travels in the collection
   */
  sortItems () {
    for (let t of this.items) {
      t.sortSections()
    }
  }

  countItems (testFunction, param = false) {
    let nb = 0
    if (param) {
      for (let travel of this.items) {
        if (testFunction(travel)) {
          nb += parseInt(travel.amount)
        }
      }
    } else {
      for (let travel of this.items) {
        if (testFunction(travel)) {
          nb++
        }
      }
    }
    return nb
  }

  reduceItems () {
    return Travel.reduce(this.items)
  }
}
