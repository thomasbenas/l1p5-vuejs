/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import HEATING_FACTORS from '@/../data/heatingFactors.json'
import ELECTRICITY_FACTORS from '@/../data/electricityFactors.json'
import REFRIGERANTS_FACTORS from '@/../data/refrigerantsFactors.json'
import {
  DetailedCarbonIntensity,
  DetailedCarbonIntensities,
  CarbonIntensity,
  CarbonIntensities
} from '@/models/carbon/CarbonIntensity.js'
import EmissionFactor from '@/models/carbon/EmissionFactor.js'

const URBAN_NETWORK = 'urban.network'
// Average mass to LHV conversion factor for heating wood (in kWh/kg)
const WOOD_CONVERSION_FACTOR = 4.0
const ENERGETIC_CLASSES = ['A', 'B', 'C', 'D', 'E', 'F', 'G']

export default class Building {
  constructor (id, name, area, share, heatings,
    refrigerants, electricity, selfProduction, selfConsumption, site, source = null, sourceRefrigerant = null) {
    this.id = id
    this.name = name
    this.area = area
    this.share = share
    this.heatings = heatings
    this.refrigerants = refrigerants
    this.electricity = electricity
    this.selfProduction = selfProduction
    this.selfConsumption = selfConsumption
    this.site = site
    this.source = source
    this.sourceRefrigerant = sourceRefrigerant
  }

  get score () {
    let score = 1
    let isElectricHeating = this.heatings.map(obj => obj.type).includes('electric')
    if ((!this.getTotalHeatingConsumption() && !isElectricHeating) || !this.getElectricityConsumption()) {
      score = 2
    }
    for (let heating of this.heatings) {
      if (this.getHeatingConsumption(heating) && !heating.type) {
        score = 2
      }
    }
    return score
  }

  isOther () {
    return false
  }

  isValid () {
    return this.score === 1
  }

  isIncomplete () {
    return false
  }

  isInvalid () {
    return this.score === 2
  }

  getTotalHeatingConsumption () {
    var consumption = 0
    for (let heating of this.heatings) {
      if (heating.type && heating.type.startsWith('wood')) {
        consumption += Math.round(this.getHeatingConsumption(heating) * WOOD_CONVERSION_FACTOR)
      } else {
        consumption += Math.round(this.getHeatingConsumption(heating))
      }
    }
    return consumption
  }

  getHeatingConsumption (heating) {
    let consumption = 0
    if (heating.isMonthly) {
      consumption += parseInt(heating.january) + parseInt(heating.february)
      consumption += parseInt(heating.march) + parseInt(heating.april)
      consumption += parseInt(heating.may) + parseInt(heating.june)
      consumption += parseInt(heating.july) + parseInt(heating.august)
      consumption += parseInt(heating.septembre) + parseInt(heating.octobre)
      consumption += parseInt(heating.novembre) + parseInt(heating.decembre)
    } else {
      consumption = heating.total
    }
    return consumption * this.share / 100
  }

  getEnergeticValues (year) {
    let intensity = 0
    let consumption = 0
    let eperformance = 0
    let iperformance = 0
    for (let heating of this.heatings) {
      if (heating.type !== 'electric') {
        intensity += this.getHeatingCarbonIntensity(heating, year).intensity
      }
    }
    consumption += this.getTotalHeatingConsumption()
    eperformance = (consumption * 100) / (this.area * this.share)
    iperformance = (intensity * 100) / (this.area * this.share)
    return [
      Building.getEnergyClass(eperformance),
      eperformance,
      consumption,
      iperformance,
      intensity
    ]
  }

  getElectricityConsumption () {
    let consumption = 0
    if (this.electricity.isMonthly) {
      consumption += parseInt(this.electricity.january) + parseInt(this.electricity.february)
      consumption += parseInt(this.electricity.march) + parseInt(this.electricity.april)
      consumption += parseInt(this.electricity.may) + parseInt(this.electricity.june)
      consumption += parseInt(this.electricity.july) + parseInt(this.electricity.august)
      consumption += parseInt(this.electricity.septembre) + parseInt(this.electricity.octobre)
      consumption += parseInt(this.electricity.novembre) + parseInt(this.electricity.decembre)
    } else {
      consumption = this.electricity.total
    }
    if (this.selfProduction) {
      consumption = (consumption - this.selfConsumption) * this.share / 100
      if (consumption < 0) { consumption = 0 }
    } else {
      consumption = consumption * this.share / 100
    }
    return consumption
  }

  getRawElectricityConsumption () {
    let consumption = 0
    if (this.electricity.isMonthly) {
      consumption += parseInt(this.electricity.january) + parseInt(this.electricity.february)
      consumption += parseInt(this.electricity.march) + parseInt(this.electricity.april)
      consumption += parseInt(this.electricity.may) + parseInt(this.electricity.june)
      consumption += parseInt(this.electricity.july) + parseInt(this.electricity.august)
      consumption += parseInt(this.electricity.septembre) + parseInt(this.electricity.octobre)
      consumption += parseInt(this.electricity.novembre) + parseInt(this.electricity.decembre)
    } else {
      consumption = this.electricity.total
    }
    return consumption
  }

  getEmissionFactor (type, subtype, year) {
    let ef = EmissionFactor.createFromObj()
    if (type === 'heatings') {
      if (subtype === 'naturalgas' || subtype === 'propane') {
        ef = EmissionFactor.createFromObj(HEATING_FACTORS['gas'][subtype])
      } else if (subtype === 'heating.oil') {
        ef = EmissionFactor.createFromObj(HEATING_FACTORS['heating.oil'][subtype])
      } else if (subtype === 'biomethane') {
        ef = EmissionFactor.createFromObj(HEATING_FACTORS['biomethane'][subtype])
      } else if (subtype.startsWith('wood')) {
        ef = EmissionFactor.createFromObj(HEATING_FACTORS['wood.heating'][subtype])
      } else {
        ef = EmissionFactor.createFromObj(HEATING_FACTORS['urban.network'][subtype])
      }
    } else if (type === 'electricity') {
      if (subtype in ELECTRICITY_FACTORS) {
        ef = EmissionFactor.createFromObj(ELECTRICITY_FACTORS[subtype])
      }
    } else if (type === 'refrigerants') {
      ef = EmissionFactor.createFromObj(REFRIGERANTS_FACTORS[subtype])
    }
    return ef.getFactor(year)
  }

  getHeatingCarbonIntensity (heating, year) {
    let consumption = this.getHeatingConsumption(heating)
    let intensity = new DetailedCarbonIntensity()
    if (heating.type === Building.urbanNetwork) {
      let ef = this.getEmissionFactor('heatings', heating.urbanNetwork, year)
      intensity = new DetailedCarbonIntensity(
        0.0,
        0.0,
        0.0,
        0.0,
        consumption * ef.total.total,
        consumption * ef.total.total * ef.total.uncertainty,
        0.0,
        0.0,
        0.0,
        0.0,
        ef.group
      )
    } else if (heating.type !== null && heating.type !== 'electric') {
      let ef = this.getEmissionFactor('heatings', heating.type, year)
      intensity = new DetailedCarbonIntensity(
        consumption * ef.combustion.co2,
        consumption * ef.combustion.ch4,
        consumption * ef.combustion.n2o,
        consumption * ef.combustion.others,
        consumption * ef.combustion.total,
        consumption * ef.combustion.total * ef.combustion.uncertainty,
        consumption * ef.upstream.total,
        consumption * ef.upstream.total * ef.upstream.uncertainty,
        0.0,
        0.0,
        ef.group
      )
    }
    return intensity
  }

  getElectricityCarbonIntensity (area, country, year) {
    let ef = this.getEmissionFactor('electricity', country + '.' + area, year)
    if (ef === null && country) {
      ef = this.getEmissionFactor('electricity', country, year)
    } else {
      ef = this.getEmissionFactor('electricity', 'FR', year)
    }
    let consumption = this.getElectricityConsumption()
    return new DetailedCarbonIntensity(
      0.0,
      0.0,
      0.0,
      0.0,
      consumption * ef.combustion.total,
      consumption * ef.combustion.total * ef.combustion.uncertainty,
      consumption * ef.upstream.total,
      consumption * ef.upstream.total * ef.upstream.uncertainty,
      0.0,
      0.0,
      ef.group
    )
  }

  getRefrigerantsCarbonIntensity (year) {
    let intensities = new CarbonIntensities()
    for (let gas of this.refrigerants) {
      let ef = this.getEmissionFactor('refrigerants', gas.name, year)
      intensities.add(new CarbonIntensity(
        gas.total * ef.total.total,
        gas.total * ef.total.total * ef.total.uncertainty,
        null,
        null,
        ef.group
      ))
    }
    return intensities
  }

  toDatabase () {
    return {
      'id': this.id,
      'name': this.name,
      'area': this.area,
      'share': this.share,
      'heatings': this.heatings,
      'refrigerants': this.refrigerants,
      'electricity': this.electricity,
      'selfProduction': this.selfProduction,
      'selfConsumption': this.selfConsumption,
      'site': this.site
    }
  }

  static get urbanNetwork () {
    return URBAN_NETWORK
  }

  static getHeatingSystems (object = false) {
    let systems = []
    for (let key of Object.keys(HEATING_FACTORS)) {
      if (key !== URBAN_NETWORK) {
        for (let key2 of Object.keys(HEATING_FACTORS[key])) {
          let lastYear = Math.max(...Object.keys(HEATING_FACTORS[key][key2].decomposition))
          systems.push({
            name: key2,
            value: HEATING_FACTORS[key][key2].decomposition[lastYear].combustion.total
          })
        }
      }
    }
    systems = systems.sort((a, b) => {
      return a.value > b.value ? 1 : -1
    })
    if (!object) {
      systems = systems.map(val => val.name)
    }
    return systems
  }

  static getHeatingSystemsWithCustom (buildings, year = null, object = false) {
    let systems = Building.getHeatingSystems(true)
    let efs = []
    let ef = null
    for (let building of buildings) {
      for (let heating of building.heatings) {
        if (heating.type || heating.urbanNetwork) {
          if (heating.type !== 'electric' && building.getHeatingConsumption(heating) > 0) {
            if (heating.type === Building.urbanNetwork) {
              ef = building.getEmissionFactor('heatings', heating.urbanNetwork, year)
            } else {
              ef = building.getEmissionFactor('heatings', heating.type, year)
            }
            efs.push(ef.total.total)
          }
        }
      }
    }
    systems.push({
      name: 'current',
      value: efs.reduce((sum, a) => sum + a, 0) / efs.length
    })
    systems = systems.sort((a, b) => {
      return a.value > b.value ? 1 : -1
    })
    if (!object) {
      systems = systems.map(val => val.name)
    }
    return systems
  }
  static getHeatingSystemsFromData (buildings) {
    let allSystems = []
    for (let building of buildings) {
      for (let heating of building.heatings) {
        if (heating.type !== 'electric' && building.getHeatingConsumption(heating) > 0) {
          allSystems.push(heating.type)
        }
      }
    }
    allSystems = Array.from(new Set(allSystems))
    return allSystems
  }

  static includeCurrent (buildings) {
    let includeCurrent = false
    let allSystems = Building.getHeatingSystemsFromData(buildings)
    if (allSystems.length === 1) {
      if (!Building.getHeatingSystems().includes(allSystems[0])) {
        includeCurrent = true
      }
    } else {
      includeCurrent = true
    }
    return includeCurrent
  }

  static get ENERGETIC_CLASSES () {
    return ENERGETIC_CLASSES
  }

  static getEnergyClass (ratio) {
    function between (x, min, max) {
      return x > min && x <= max
    }
    if (ratio) {
      let eclass = 6
      ratio <= 70 ? eclass = 0
        : between(ratio, 70, 110) ? eclass = 1
          : between(ratio, 110, 180) ? eclass = 2
            : between(ratio, 180, 250) ? eclass = 3
              : between(ratio, 250, 330) ? eclass = 4
                : between(ratio, 330, 420) ? eclass = 5
                  : eclass = 6
      return ENERGETIC_CLASSES[eclass]
    } else {
      return '-'
    }
  }

  static getEnergeticValues (buildings, year) {
    let intensities = []
    let consumptions = []
    let eperformances = []
    let iperformances = []
    for (let building of buildings) {
      let values = building.getEnergeticValues(year)
      intensities.push(values[4])
      consumptions.push(values[2])
      eperformances.push(values[1])
      iperformances.push(values[3])
    }
    let mconsumption = Math.round(consumptions.reduce((sum, a) => sum + a, 0) / consumptions.length)
    let meperformance = Math.round(eperformances.reduce((sum, a) => sum + a, 0) / eperformances.length)
    let mintensity = Math.round(intensities.reduce((sum, a) => sum + a, 0) / intensities.length)
    let miperformance = Math.round(iperformances.reduce((sum, a) => sum + a, 0) / iperformances.length)
    return [
      Building.getEnergyClass(meperformance),
      meperformance,
      mconsumption,
      miperformance,
      mintensity
    ]
  }

  static getRefrigerants () {
    let refrigerants = Object.keys(REFRIGERANTS_FACTORS)
    return Array.from(new Set(refrigerants))
  }

  static getUrbanNetworks () {
    let urbanNetworks = []
    for (let network of Object.keys(HEATING_FACTORS['urban.network'])) {
      urbanNetworks.push({
        'network': network,
        'description_FR': HEATING_FACTORS['urban.network'][network]['description_FR'],
        'description_EN': HEATING_FACTORS['urban.network'][network]['description_EN']
      })
    }
    return urbanNetworks
  }

  static getHeatings () {
    let heatings = []
    for (let subtype of Object.keys(HEATING_FACTORS)) {
      if (subtype !== 'urban.network') {
        for (let subsubtype of Object.keys(HEATING_FACTORS[subtype])) {
          heatings.push(subsubtype)
        }
      }
    }
    heatings.unshift(URBAN_NETWORK)
    heatings.unshift('electric')
    return Array.from(new Set(heatings))
  }

  static getHeatingUnit (type) {
    let unit = null
    if (type === 'naturalgas' || type === 'propane') {
      unit = HEATING_FACTORS['gas'][type].unit
    } else if (type === 'heating.oil') {
      unit = HEATING_FACTORS['heating.oil'][type].unit
    } else if (type === 'biomethane') {
      unit = HEATING_FACTORS['biomethane'][type].unit
    } else if (type.startsWith('wood')) {
      unit = HEATING_FACTORS['wood.heating'][type].unit
    } else {
      unit = HEATING_FACTORS['urban.network'][type].unit
    }
    return unit
  }

  static getDescription (type, subtype, lang = 'FR') {
    let desc = ''
    if (type === 'heatings') {
      if (subtype === 'naturalgas' || subtype === 'propane') {
        desc = HEATING_FACTORS['gas'][subtype]['description_' + lang]
      } else if (subtype === 'heating.oil') {
        desc = HEATING_FACTORS['heating.oil'][subtype]['description_' + lang]
      } else if (subtype === 'biomethane') {
        desc = HEATING_FACTORS['biomethane'][subtype]['description_' + lang]
      } else if (subtype.startsWith('wood')) {
        desc = HEATING_FACTORS['wood.heating'][subtype]['description_' + lang]
      } else {
        desc = HEATING_FACTORS['urban.network'][subtype]['description_' + lang]
      }
    } else if (type === 'electricity') {
      desc = ELECTRICITY_FACTORS[subtype]['description_' + lang]
    } else if (type === 'refrigerants') {
      desc = REFRIGERANTS_FACTORS[subtype]['description_' + lang]
    }
    return desc
  }

  static createElectricity () {
    return {
      id: null,
      january: 0,
      february: 0,
      march: 0,
      april: 0,
      may: 0,
      june: 0,
      july: 0,
      august: 0,
      septembre: 0,
      octobre: 0,
      novembre: 0,
      decembre: 0,
      total: 0,
      isMonthly: true
    }
  }

  static createHeating () {
    return {
      id: null,
      type: null,
      urbanNetwork: null,
      january: 0,
      february: 0,
      march: 0,
      april: 0,
      may: 0,
      june: 0,
      july: 0,
      august: 0,
      septembre: 0,
      octobre: 0,
      novembre: 0,
      decembre: 0,
      total: 0,
      isMonthly: true,
      isOwnedByLab: false
    }
  }

  static createFromObj (building) {
    return new Building(
      building.id,
      building.name,
      building.area,
      building.share,
      building.heatings,
      building.refrigerants,
      building.electricity,
      building.selfProduction,
      building.selfConsumption,
      building.site,
      building.source
    )
  }

  static compute (buildings, area, country, year) {
    let heatingc = new DetailedCarbonIntensities()
    let heatingu = new DetailedCarbonIntensities()
    let heatingi = new CarbonIntensities()
    let electricityi = new DetailedCarbonIntensities()
    let refrigerantsi = new CarbonIntensities()
    let intensities = new CarbonIntensities()
    let meta = []
    for (let building of buildings) {
      // heating carbon intensity
      let heatingIntensity = new DetailedCarbonIntensities()
      for (let heating of building.heatings) {
        let cheatingIntensity = building.getHeatingCarbonIntensity(heating, year)
        heatingIntensity.add(cheatingIntensity)
        if (heating.isOwnedByLab) {
          heatingc.add(cheatingIntensity)
        } else {
          heatingu.add(cheatingIntensity)
        }
        heatingi.add(cheatingIntensity)
      }
      // electricity carbon intensity
      let electricityIntensity = building.getElectricityCarbonIntensity(area, country, year)
      electricityi.add(electricityIntensity)

      // refrigerants carbon intensity
      let refrigerantsIntensity = building.getRefrigerantsCarbonIntensity(year)
      refrigerantsi.add(refrigerantsIntensity)

      // total carbon intensity
      intensities.add(heatingIntensity)
      intensities.add(electricityIntensity)
      intensities.add(refrigerantsIntensity)

      // all data for export
      meta.unshift({
        'name': building.name,
        'site': building.site,
        'area': building.area,
        'share': building.share,
        'selfProduction': building.selfProduction,
        'selfConsumption': building.selfConsumption,
        'heatings': building.heatings,
        'refrigerants': building.refrigerants,
        'electricity': building.electricity,
        'heatingIntensity': heatingIntensity.sum(),
        'electricityIntensity': electricityIntensity,
        'refrigerantsIntensity': refrigerantsIntensity.sum()
      })
    }
    return {
      'intensity': intensities.sum(),
      'heatings': {
        'controled': heatingc.sum(),
        'uncontroled': heatingu.sum(),
        'intensity': heatingi.sum()
      },
      'electricity': electricityi.sum(),
      'refrigerants': refrigerantsi.sum(),
      'meta': meta
    }
  }
}
