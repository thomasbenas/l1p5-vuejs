/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import Modules from '@/models/Modules.js'
import {
  CarbonIntensity,
  CarbonIntensities
} from '@/models/carbon/CarbonIntensity.js'

/** Represent the footprint synthesis of a ghgi (travels, building). */
export default class Synthesis {
  /**
   *
   * Create a new Synthesis
   *
   * @param {number} intensity intensity of carbon in kgCO2eq per unit
   *                            exact unit differs from the activity type (e.g per km, per litre)
   * @param {number} uncertainty uncertainty of the intensity in kgCO2eq per unit
   * @param {number=} intensitywc intensity of carbon in kgCO2eq per unit of contrails (plane travels)
   * @param {number=} uncertaintywc uncertainty of the intensity in kgCO2eq per unit of the contrails intensity (plane travels)
   * @param {number=} group emission group (e.g fossil.fuels for buildings)
   *
   */
  constructor (
    heatings,
    electricity,
    refrigerants,
    commutes,
    travels,
    vehicles,
    devices,
    purchases
  ) {
    this.heatings = CarbonIntensity.createFromObj(heatings)
    this.electricity = CarbonIntensity.createFromObj(electricity)
    this.refrigerants = CarbonIntensity.createFromObj(refrigerants)
    this.commutes = CarbonIntensity.createFromObj(commutes)
    this.travels = CarbonIntensity.createFromObj(travels)
    this.vehicles = CarbonIntensity.createFromObj(vehicles)
    this.devices = CarbonIntensity.createFromObj(devices)
    this.purchases = CarbonIntensity.createFromObj(purchases)
  }

  get buildings () {
    let intensities = new CarbonIntensities()
    intensities.add(this.electricity)
    intensities.add(this.heatings)
    intensities.add(this.refrigerants)
    return intensities.sum()
  }

  getIntensity (withContrails) {
    let intensity = 0.0
    for (let module of Modules.MODULES) {
      intensity += this[module].getIntensity(withContrails)
    }
    return intensity
  }

  getUncertainty (withContrails) {
    let uncertainty = 0.0
    for (let module of Modules.MODULES) {
      uncertainty += this[module].getUncertainty(withContrails)
    }
    return uncertainty
  }

  static createFromObj (item) {
    let heatings = new CarbonIntensity()
    let electricity = new CarbonIntensity()
    let refrigerants = new CarbonIntensity()
    let commutes = new CarbonIntensity()
    let travels = new CarbonIntensity()
    let vehicles = new CarbonIntensity()
    let devices = new CarbonIntensity()
    let purchases = new CarbonIntensity()
    if (item) {
      heatings = CarbonIntensity.createFromObj(item.heatings)
      electricity = CarbonIntensity.createFromObj(item.electricity)
      refrigerants = CarbonIntensity.createFromObj(item.refrigerants)
      commutes = CarbonIntensity.createFromObj(item.commutes)
      travels = CarbonIntensity.createFromObj(item.travels)
      vehicles = CarbonIntensity.createFromObj(item.vehicles)
      devices = CarbonIntensity.createFromObj(item.devices)
      purchases = CarbonIntensity.createFromObj(item.purchases)
    }
    return new Synthesis(
      heatings,
      electricity,
      refrigerants,
      commutes,
      travels,
      vehicles,
      devices,
      purchases
    )
  }
}
