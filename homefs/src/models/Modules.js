/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

// Modules definitions
const BUILDINGS = 'buildings'
const HEATINGS = 'heatings'
const ELECTRICITY = 'electricity'
const REFRIGERANTS = 'refrigerants'
const PURCHASES = 'purchases'
const DEVICES = 'devices'
const VEHICLES = 'vehicles'
const TRAVELS = 'travels'
const COMMUTES = 'commutes'

// Modules colors definitions
const HEATING_COLOR = '#c84896'
const ELECTRICITY_COLOR = '#e88bc3'
const REFRIGERANTS_COLOR = '#fdc2e5'
const VEHICLES_COLOR = '#fd8e62'
const TRAVELS_COLOR = '#67c3a5'
const COMMUTES_COLOR = '#8ea0cc'
const DEVICES_COLOR = '#a6d953'
const PURCHASES_COLOR = '#ffda2c'

// Modules icons definitions
const BUILDINGS_ICON = 'building'
const PURCHASES_ICON = 'eur'
const DEVICES_ICON = 'desktop'
const VEHICLES_ICON = 'car'
const TRAVELS_ICON = 'train'
const COMMUTES_ICON = 'bicycle'

export default class Modules {
  static get BUILDINGS () { return BUILDINGS }
  static get HEATINGS () { return HEATINGS }
  static get ELECTRICITY () { return ELECTRICITY }
  static get REFRIGERANTS () { return REFRIGERANTS }
  static get PURCHASES () { return PURCHASES }
  static get DEVICES () { return DEVICES }
  static get VEHICLES () { return VEHICLES }
  static get TRAVELS () { return TRAVELS }
  static get COMMUTES () { return COMMUTES }
  static get MODULES () {
    return [
      HEATINGS,
      ELECTRICITY,
      REFRIGERANTS,
      PURCHASES,
      DEVICES,
      VEHICLES,
      TRAVELS,
      COMMUTES
    ]
  }
  static get PACKED_MODULES () {
    return [
      BUILDINGS,
      PURCHASES,
      DEVICES,
      VEHICLES,
      TRAVELS,
      COMMUTES
    ]
  }
  static get NOT_PACKED_MODULES () {
    return [
      HEATINGS,
      ELECTRICITY,
      REFRIGERANTS
    ]
  }

  static get HEATING_COLOR () { return HEATING_COLOR }
  static get ELECTRICITY_COLOR () { return ELECTRICITY_COLOR }
  static get REFRIGERANTS_COLOR () { return REFRIGERANTS_COLOR }
  static get VEHICLES_COLOR () { return VEHICLES_COLOR }
  static get TRAVELS_COLOR () { return TRAVELS_COLOR }
  static get COMMUTES_COLOR () { return COMMUTES_COLOR }
  static get DEVICES_COLOR () { return DEVICES_COLOR }
  static get PURCHASES_COLOR () { return PURCHASES_COLOR }
  static get COLORS () {
    return {
      [Modules.HEATINGS]: Modules.HEATING_COLOR,
      [Modules.ELECTRICITY]: Modules.ELECTRICITY_COLOR,
      [Modules.REFRIGERANTS]: Modules.REFRIGERANTS_COLOR,
      [Modules.PURCHASES]: Modules.PURCHASES_COLOR,
      [Modules.DEVICES]: Modules.DEVICES_COLOR,
      [Modules.VEHICLES]: Modules.VEHICLES_COLOR,
      [Modules.TRAVELS]: Modules.TRAVELS_COLOR,
      [Modules.COMMUTES]: Modules.COMMUTES_COLOR
    }
  }
  static get BUILDINGS_ICON () { return BUILDINGS_ICON }
  static get VEHICLES_ICON () { return VEHICLES_ICON }
  static get TRAVELS_ICON () { return TRAVELS_ICON }
  static get COMMUTES_ICON () { return COMMUTES_ICON }
  static get DEVICES_ICON () { return DEVICES_ICON }
  static get PURCHASES_ICON () { return PURCHASES_ICON }
  static get ICONS () {
    return {
      [Modules.BUILDINGS]: Modules.BUILDINGS_ICON,
      [Modules.PURCHASES]: Modules.PURCHASES_ICON,
      [Modules.DEVICES]: Modules.DEVICES_ICON,
      [Modules.VEHICLES]: Modules.VEHICLES_ICON,
      [Modules.TRAVELS]: Modules.TRAVELS_ICON,
      [Modules.COMMUTES]: Modules.COMMUTES_ICON
    }
  }
}
