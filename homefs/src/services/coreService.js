import api from '@/services/api'

export default {
  getSettings (payload) {
    return api.post(`get_settings/`, payload)
      .then(response => response.data)
  },
  saveSettings (payload) {
    return api.post(`save_settings/`, payload)
      .then(response => response.data)
  },
  getAdministrations () {
    return api.get(`get_administrations/`)
      .then(response => response.data)
  },
  getDisciplines () {
    return api.get(`get_disciplines/`)
      .then(response => response.data)
  },
  saveLaboratory (payload) {
    return api.post(`save_laboratory/`, payload)
      .then(response => response.data)
  },
  getLaboratory (payload) {
    return api.get(`get_laboratory/`)
      .then(response => response.data)
  }
}
