/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import scenarioService from '@/services/scenarioService'
import Scenario from '@/models/scenario/Scenario.js'

function initialState () {
  return {
    item: {}
  }
}

const state = initialState()

const getters = {
  item: state => state.item
}

const actions = {
  set ({ dispatch, rootGetters, commit, state }, uuid) {
    return scenarioService.getScenario({
      uuid: uuid
    })
      .then(scenario => {
        let module = null
        const settings = rootGetters['core/setting']('commute')
        let ghgi = rootGetters['admin/allGHGI'].filter(obj => obj.id === scenario.ghgi.id)[0]
        commit('SET', scenario)
        commit('SET_GHGI', ghgi)
        commit('COMPUTE', { settings, module })
        return scenario
      })
      .catch(error => {
        throw error
      })
  },
  setEmpty ({ commit }) {
    commit('SET_EMPTY')
  },
  setFromUUID ({ dispatch, rootGetters, commit, state }, uuid) {
    return scenarioService.getScenarioFromUUID({
      uuid: uuid
    })
      .then(data => {
        let module = null
        const settings = rootGetters['core/setting']('commute')
        dispatch('admin/setGHGI', data.ghgi, { root: true })
        let ghgi = rootGetters['admin/allGHGI'].filter(obj => obj.id === data.scenario.ghgi.id)[0]
        commit('SET', data.scenario)
        commit('SET_GHGI', ghgi)
        commit('COMPUTE', { settings, module })
        return data
      })
      .catch(error => {
        throw error
      })
  },
  setGHGI ({ commit }, ghgi) {
    commit('SET_GHGI', ghgi)
  },
  addRule ({ commit }, rule) {
    commit('ADD_RULE', rule)
  },
  removeRule ({ commit }, rule) {
    commit('REMOVE_RULE', rule)
  },
  updateLevel1 ({ commit }, data) {
    commit('UPDATE_LEVEL1', data)
  },
  updateLevel2 ({ commit }, data) {
    commit('UPDATE_LEVEL2', data)
  },
  resetLevel2 ({ commit }, data) {
    commit('RESET_LEVEL2', data)
  },
  compute ({ dispatch, rootGetters, commit, state }, module = null) {
    const settings = rootGetters['core/setting']('commute')
    commit('COMPUTE', { settings, module })
  },
  save ({ dispatch, rootGetters, commit, state }, ghgiID) {
    return scenarioService.saveScenario({
      'scenario': state.item.toDatabase(),
      'ghgi_id': ghgiID
    })
      .then(scenario => {
        let module = null
        const settings = rootGetters['core/setting']('commute')
        dispatch('admin/getScenarios', null, { root: true })
        let ghgi = rootGetters['admin/allGHGI'].filter(obj => obj.id === scenario.ghgi.id)[0]
        commit('SET', scenario)
        commit('SET_GHGI', ghgi)
        commit('COMPUTE', { settings, module })
        return scenario
      })
      .catch(error => {
        throw error
      })
  }
}

const mutations = {
  RESET_STATE (state) {
    const iObj = initialState()
    Object.keys(iObj).forEach(key => {
      state[key] = iObj[key]
    })
  },
  SET (state, value) {
    state.item = Scenario.createFromObj(value)
  },
  SET_NAME (state, value) {
    state.item.name = value
  },
  SET_DESCRIPTION (state, value) {
    state.item.description = value
  },
  SET_TIMELINE (state, value) {
    state.item.timeline = value
  },
  SET_GHGI (state, ghgi) {
    state.item.setGHGI(ghgi)
  },
  ADD_RULE (state, rule) {
    state.item.addRule(rule)
  },
  REMOVE_RULE (state, rule) {
    state.item.removeRule(rule)
  },
  UPDATE_LEVEL1 (state, data) {
    let rule2update = state.item.rules.filter(obj => obj.id === data.id)[0]
    rule2update.level1 = data.value
  },
  UPDATE_LEVEL2 (state, data) {
    let rule2update = state.item.rules.filter(obj => obj.id === data.id)[0]
    for (let param of Object.keys(data.values)) {
      rule2update.level2[param].value = data.values[param]
    }
  },
  RESET_LEVEL2 (state, data) {
    let rule2update = state.item.rules.filter(obj => obj.id === data.id)[0]
    rule2update.resetLevel2Values()
  },
  SET_EMPTY (state) {
    state.item = new Scenario(null, null, Scenario.DEFAULT_TIMELINE, null, null, [], null)
  },
  COMPUTE (state, { settings, module = null }) {
    state.item.compute(settings, module)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
