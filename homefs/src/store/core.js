import coreService from '@/services/coreService'
import { DEFAULT_SETTINGS } from './constants'

function initialState () {
  return {
    settings: DEFAULT_SETTINGS
  }
}

const state = initialState()

const getters = {
  settings: state => state.settings,
  setting: (state) => (section, name) => {
    if (section && name) {
      return state.settings.filter(obj => obj.section === section && obj.name === name)[0]
    } else if (name) {
      return state.settings.filter(obj => obj.name === name)[0]
    } else if (section) {
      return state.settings.filter(obj => obj.section === section)
    }
  },
  sections: (state) => {
    return new Set(state.settings.map(obj => obj.section))
  }
}

const actions = {
  resetState ({ commit }) {
    commit('RESET_STATE')
  },
  getSettings ({ dispatch, rootGetters, commit, state }) {
    return coreService.getSettings({ 'settings': state.settings })
      .then(settings => {
        commit('SET_SETTINGS', settings)
        return settings
      })
      .catch(error => {
        throw error
      })
  },
  saveSettings ({ dispatch, rootGetters, commit, state }) {
    return coreService.saveSettings({ 'settings': state.settings })
      .then(settings => {
        commit('SET_SETTINGS', settings)
        return settings
      })
      .catch(error => {
        throw error
      })
  }
}

const mutations = {
  RESET_STATE (state) {
    const iObj = initialState()
    Object.keys(iObj).forEach(key => {
      state[key] = iObj[key]
    })
  },
  SET_SETTINGS (state, settings) {
    state.settings = settings
  },
  UPDATE_SETTING (state, setting) {
    let csetting = state.settings.filter(obj => obj.id === setting.id)[0]
    csetting.value = setting.value
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
