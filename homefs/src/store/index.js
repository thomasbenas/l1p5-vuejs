import Vue from 'vue'
import Vuex from 'vuex'

import authentication from './authentication'
import superadmin from './superadmin'
import admin from './admin'

import core from './core'
import ghgi from './ghgi'
import transition from './transition'
import scenario from './scenario'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    authentication,
    superadmin,
    admin,
    core,
    transition,
    scenario,
    ghgi
  }
})
