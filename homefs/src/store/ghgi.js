/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

// import GHGI from '@/models/carbon/GHGI.js'
import carbonService from '@/services/carbonService'
import GHGI from '@/models/carbon/GHGI.js'
import Modules from '@/models/Modules.js'

function initialState () {
  return {
    item: new GHGI(),
    computed: false
  }
}

const state = initialState()

const getters = {
  item: state => state.item,
  computed: state => state.computed,
  items: (state) => (module) => { return state.item[module].toArray() },
  collection: (state) => (module) => { return state.item[module] },
  hasItems: (state) => (module) => {
    return state.item[module].length > 0
  },
  otherItems: (state) => (module) => {
    return state.item[module].filter(item => item.isOther())
  },
  validItems: (state) => (module) => {
    return state.item[module].filter(item => item.isValid())
  },
  incompleteItems: (state) => (module) => {
    return state.item[module].filter(item => item.isIncomplete())
  },
  invalidItems: (state) => (module) => {
    return state.item[module].filter(item => item.isInvalid())
  },
  countItems: (state) => (data) => {
    return state.item[data.module].countItems(
      data.testFuncion,
      data.param
    )
  },
  isClonedSurvey: state => state.item.surveyCloneYear !== null,
  nMember: state => {
    return state.item.nResearcher +
      state.item.nProfessor +
      state.item.nEngineer +
      state.item.nStudent
  }
}

const actions = {
  resetState ({ commit }) {
    commit('RESET_STATE')
  },
  addItem ({ commit }, { module, item, param }) {
    commit('ADD_ITEM', { module, item, param })
  },
  updateItem ({ commit }, { module, item }) {
    commit('UPDATE_ITEM', { module, item })
  },
  updateItems ({ commit }, { module, items }) {
    commit('UPDATE_ITEMS', {
      module: module,
      items: items
    })
  },
  deleteItem ({ commit }, { module, item }) {
    commit('DELETE_ITEM', { module, item })
  },
  deleteSource ({ commit }, { module, source }) {
    commit('DELETE_SOURCE', { module, source })
  },
  deleteInvalidItems ({ commit }, module) {
    commit('DELETE_INVALID_ITEMS', module)
  },
  sortItems ({ commit }, module) {
    commit('SORT_ITEMS', module)
  },
  saveItems ({ dispatch, getters, commit, state }, module) {
    let func = null
    if (module === Modules.BUILDINGS) {
      func = carbonService.saveBuildings
    } else if (module === Modules.VEHICLES) {
      func = carbonService.saveVehicles
    } else if (module === Modules.PURCHASES) {
      func = carbonService.savePurchases
    } else if (module === Modules.DEVICES) {
      func = carbonService.saveComputerDevices
    } else if (module === Modules.TRAVELS) {
      func = carbonService.saveTravels
    } else if (module === Modules.COMMUTES) {
      func = carbonService.saveCommutes
    }
    dispatch('compute', true)
    let allData = {
      [module]: getters.collection(module).reduceItems().map(obj => obj.toDatabase()),
      'ghgi_id': getters.item.id,
      synthesis: state.item.synthesis
    }
    return func(allData)
      .then(data => {
        commit('UPDATE_ITEMS', {
          module: module,
          items: data
        })
        dispatch('forceComputation')
        return data
      })
      .catch(error => {
        throw error
      })
  },
  compute ({ dispatch, rootGetters, getters, commit }, force = false) {
    if (force) { dispatch('forceComputation') }
    if (!getters.computed) {
      const settings = rootGetters['core/setting']('commute')
      const year = getters.item.year
      commit('COMPUTE', { settings, year })
    }
  },
  saveSurveyMessage ({ getters, commit }, surveyMessage) {
    let allData = {
      'surveyMessage': surveyMessage,
      'ghgi_id': getters.item.id
    }
    return carbonService.saveSurveyMessage(allData)
      .then(data => {
        commit('UPDATE_MESSAGE', surveyMessage)
        return data
      })
      .catch(error => {
        throw error
      })
  },
  updateGHGI ({ dispatch, rootGetters, commit, state }, ghgi) {
    dispatch('admin/updateGHGI', ghgi, { root: true })
    commit('UPDATE_GHGI', ghgi)
  },
  set ({ dispatch }, id) {
    return carbonService.getGHGIConsumptions({ 'ghgi_id': id })
      .then(data => {
        dispatch('updateGHGI', data['ghgi'])
        dispatch('admin/updateLaboratory', data['ghgi'].laboratory, { root: true })
        for (let module of Modules.PACKED_MODULES) {
          dispatch('updateItems', {
            module: module,
            items: data[module]
          })
        }
        dispatch('compute', true)
        return data
      })
      .catch(error => {
        throw error
      })
  },
  setFromUUID ({ dispatch, state }, uuid) {
    return carbonService.getGHGIConsumptionsByUUID({ 'uuid': uuid })
      .then(data => {
        dispatch('updateGHGI', data['ghgi'])
        dispatch('admin/updateLaboratory', data['ghgi'].laboratory, { root: true })
        for (let module of Modules.PACKED_MODULES) {
          dispatch('updateItems', {
            module: module,
            items: data[module]
          })
        }
        dispatch('compute', true)
        return data
      })
      .catch(error => {
        throw error
      })
  },
  saveGHGI ({ dispatch, rootGetters, commit, state }, ghgi) {
    return carbonService.saveGHGI(ghgi)
      .then(data => {
        dispatch('updateGHGI', JSON.parse(JSON.stringify(data)))
        dispatch('admin/getAllGHGI', null, { root: true })
        return data
      })
      .catch(error => {
        throw error
      })
  },
  submitData ({ dispatch, rootGetters, commit, state }, data) {
    return carbonService.submitData({
      'ghgi_id': state.item.id,
      'module': data.module,
      'synthesis': data.synthesis
    })
      .then(data => {
        dispatch('updateGHGI', JSON.parse(JSON.stringify(data)))
        return data
      })
      .catch(error => {
        throw error
      })
  },
  resetGHGI ({ dispatch, rootGetters, commit, state }) {
    commit('RESET_STATE')
  },
  forceComputation ({ commit }) {
    commit('UPDATE_COMPUTED', false)
  },
  cloneSurvey ({ dispatch, rootGetters, commit, state }, surveyCloneYear) {
    let allData = {
      'surveyCloneYear': surveyCloneYear,
      'ghgi_id': state.item['id']
    }
    return carbonService.cloneSurvey(allData)
      .then(data => {
        dispatch('updateGHGI', JSON.parse(JSON.stringify(data['ghgi'])))
        dispatch('updateItems', {
          module: 'commutes',
          items: data['commutes']
        })
        return data
      })
      .catch(error => {
        throw error
      })
  },
  activateGHGISurvey ({ dispatch, rootGetters, commit, state }) {
    dispatch('compute', true)
    return carbonService.activateGHGISurvey({
      'ghgi_id': state.item.id,
      'synthesis': state.item.synthesis
    })
      .then(data => {
        dispatch('updateGHGI', JSON.parse(JSON.stringify(data)))
        return data
      })
      .catch(error => {
        throw error
      })
  }
}

const mutations = {
  RESET_STATE (state) {
    const iObj = initialState()
    Object.keys(iObj).forEach(key => {
      state[key] = iObj[key]
    })
  },
  ADD_ITEM (state, { module, item, param }) {
    state.item[module].add(item, param)
  },
  UPDATE_ITEM (state, { module, item }) {
    state.item[module].update(item)
  },
  UPDATE_ITEMS (state, data) {
    state.item[data.module].reset()
    for (let item of data.items) {
      state.item[data.module].add(item)
    }
  },
  DELETE_ITEM (state, { module, item }) {
    state.item[module].delete(item)
  },
  DELETE_SOURCE (state, { module, source }) {
    if (source) {
      state.item[module].deleteSource(source)
    } else {
      state.item[module].reset()
    }
  },
  DELETE_INVALID_ITEMS (state, module) {
    state.item[module].deleteInvalid()
  },
  SORT_ITEMS (state, module) {
    state.item[module].sortItems()
  },
  COMPUTE (state, { settings, year }) {
    state.item.compute(settings, year)
    state.computed = true
  },
  UPDATE_GHGI (state, ghgi) {
    state.item.id = ghgi.id
    state.item.uuid = ghgi.uuid
    state.item.laboratory = ghgi.laboratory
    state.item.buildingsSubmitted = ghgi.buildingsSubmitted
    state.item.commutesSubmitted = ghgi.commutesSubmitted
    state.item.devicesSubmitted = ghgi.devicesSubmitted
    state.item.travelsSubmitted = ghgi.travelsSubmitted
    state.item.vehiclesSubmitted = ghgi.vehiclesSubmitted
    state.item.purchasesSubmitted = ghgi.purchasesSubmitted
    state.item.surveyActive = ghgi.surveyActive
    state.item.surveyCloneYear = ghgi.surveyCloneYear
    state.item.year = ghgi.year
    state.item.surveyMessage = ghgi.surveyMessage
    state.item.nResearcher = ghgi.nResearcher
    state.item.nProfessor = ghgi.nProfessor
    state.item.nEngineer = ghgi.nEngineer
    state.item.nStudent = ghgi.nStudent
    state.item.budget = ghgi.budget
  },
  UPDATE_COMPUTED (state, computed) {
    state.computed = computed
  },
  UPDATE_YEAR (state, year) {
    state.item.year = year
  },
  UPDATE_MESSAGE (state, surveyMessage) {
    state.item.surveyMessage = surveyMessage
  },
  UPDATE_NRESEARCHER (state, nResearcher) {
    state.item.nResearcher = nResearcher
  },
  UPDATE_NPROFESSOR (state, nProfessor) {
    state.item.nProfessor = nProfessor
  },
  UPDATE_NENGINEER (state, nEngineer) {
    state.item.nEngineer = nEngineer
  },
  UPDATE_NSTUDENT (state, nStudent) {
    state.item.nStudent = nStudent
  },
  UPDATE_BUDGET (state, budget) {
    state.item.budget = budget
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
