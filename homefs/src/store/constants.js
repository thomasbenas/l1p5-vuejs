export const DEFAULT_SETTINGS = [
  {
    'section': 'global',
    'name': 'GEONAMES_USERNAME',
    'value': 'l1p5dev',
    'type': 1
  },
  {
    'section': 'commute',
    'name': 'NUMBER_OF_WORKED_WEEK.default',
    'value': '41',
    'type': 2
  },
  {
    'section': 'commute',
    'name': 'NUMBER_OF_WORKED_WEEK.2020',
    'value': '27',
    'type': 2
  },
  {
    'section': 'commute',
    'name': 'NUMBER_OF_WORKED_WEEK.2021',
    'value': '37',
    'type': 2
  }
]
