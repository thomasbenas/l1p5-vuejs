
import carbonService from '@/services/carbonService'
import scenarioService from '@/services/scenarioService'
import transitionService from '@/services/transitionService'
import Scenario from '@/models/scenario/Scenario.js'
import GHGI from '@/models/carbon/GHGI.js'

function initialState () {
  return {
    allGHGI: [],
    scenarios: [],
    allInitiatives: []
  }
}

const state = initialState()

const getters = {
  allGHGI: state => state.allGHGI,
  scenarios: state => state.scenarios,
  allInitiatives: state => state.allInitiatives
}

const actions = {
  resetState ({ commit }) {
    commit('RESET_STATE')
  },
  getAllGHGI ({ dispatch, rootGetters, commit, state }, data) {
    let toCompute = false
    return carbonService
      .getAllGHGIAdmin(data)
      .then((allGHGI) => {
        const settings = rootGetters['core/setting']('commute')
        commit('SET_ALL_GHGI', { allGHGI, settings, toCompute })
        return allGHGI
      })
      .catch(error => {
        throw error
      })
  },
  getScenarios ({ dispatch, rootGetters, commit, state }, data) {
    return scenarioService.getScenariosAdmin(data)
      .then(scenarios => {
        commit('SET_SCENARIOS', scenarios)
        return scenarios
      })
      .catch(error => {
        throw error
      })
  },
  unsubmitData ({ dispatch, rootGetters, commit, state }, data) {
    let toCompute = false
    return carbonService
      .unsubmitData(data)
      .then((allGHGI) => {
        const settings = rootGetters['core/setting']('commute')
        commit('SET_ALL_GHGI', { allGHGI, settings, toCompute })
        return allGHGI
      })
      .catch(error => {
        throw error
      })
  },
  getAllGHGIWithConsumptions ({ dispatch, rootGetters, commit, state }, data) {
    let toCompute = true
    return carbonService
      .getAllGHGIWithConsumptionsAdmin(data)
      .then((allGHGI) => {
        const settings = rootGetters['core/setting']('commute')
        commit('SET_ALL_GHGI', { allGHGI, settings, toCompute })
        return allGHGI
      })
      .catch(error => {
        throw error
      })
  },
  getAllInitiatives ({ commit }) {
    return transitionService.getInitiativesAdmin()
      .then(allInitiatives => {
        commit('SET_ALL_INITIATIVES', allInitiatives)
        return allInitiatives
      })
      .catch(error => {
        throw error
      })
  },
  valdidateInitiative ({ commit }, initiative) {
    return transitionService.validateInitiative(initiative)
      .then(allInitiatives => {
        commit('SET_ALL_INITIATIVES', allInitiatives)
        return allInitiatives
      })
      .catch(error => {
        throw error
      })
  }
}

const mutations = {
  RESET_STATE (state) {
    const iObj = initialState()
    Object.keys(iObj).forEach(key => {
      state[key] = iObj[key]
    })
  },
  SET_ALL_GHGI (state, { allGHGI, settings, toCompute }) {
    state.allGHGI = []
    for (let ghgi of allGHGI) {
      let cghgi = GHGI.createFromObj(ghgi)
      if (toCompute) {
        cghgi.compute(settings)
      }
      state.allGHGI.unshift(cghgi)
    }
  },
  SET_SCENARIOS (state, scenarios) {
    state.scenarios = []
    for (let scenario of scenarios) {
      let csenario = Scenario.createFromObj(scenario)
      state.scenarios.unshift(csenario)
    }
  },
  SET_ALL_INITIATIVES (state, allInitiatives) {
    state.allInitiatives = allInitiatives
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
