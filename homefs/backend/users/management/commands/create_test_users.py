from django.core.management.base import BaseCommand, CommandError
from backend.test_utils import (
    delete_user,
    ensure_user_created,
    ensure_lab_created,
    create_test_users,
)


class Command(BaseCommand):
    help = """Create a pool of users.

    l1p5-test-adming@l1p5.org: super powered user !
    l1p5-test-lab-XXX@l1p5.org: user with a lab associated
    l1p5-test-nolab-XXX@l1p5.org: user with no lab associated

    Note: this deletes any existing users (based on the email)
    """

    def handle(self, *args, **options):
        create_test_users()
