from django.test import Client
from django.test import TestCase
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework import status

from backend.carbon.models import GHGI
from backend.test_utils import ensure_user_created, ensure_lab_created


REQUEST_KWARGS = dict(content_type="application/json")


def test_intensity(i: int = 0):
    """util fonction that builds an intensity."""
    return {
        "intensity": i,
        "intensitywc": i + 1,
        "uncertainty": i + 2,
        "uncertaintywc": i + 3,
    }


ALL_MODULES = ["buildings", "commutes", "devices", "purchases", "travels", "vehicles"]
ALL_MODULES_CARBON_INTENSITY = [
    "heatings",
    "refrigerants",
    "electricity",
    "commutes",
    "devices",
    "purchases",
    "travels",
    "vehicles",
]


class TestPermissions(TestCase):
    def setUp(self) -> None:
        # create a user with a ghgi
        self.victim = ensure_user_created("victim@l1p5.org")
        refresh = RefreshToken.for_user(self.victim)
        # in django >= 4.2 we could use the headers for that
        self.api_victim = Client(
            HTTP_AUTHORIZATION=f"Bearer {refresh.access_token}",
        )

        self.laboratory = ensure_lab_created(self.victim)
        self.ghgi = GHGI(
            laboratory=self.laboratory,
            year=2042,
            nResearcher=1,
            nProfessor=1,
            nEngineer=1,
            nStudent=1,
            budget=int(10e6),
        )
        self.ghgi.save()

        # create a second user (attacker)
        self.attacker = ensure_user_created(email="attacker@l1p5.org")
        refresh = RefreshToken.for_user(self.attacker)
        # in django >= 4.2 we could use the headers for that
        self.api_attacker = Client(
            HTTP_AUTHORIZATION=f"Bearer {refresh.access_token}",
        )

    def test_save_someoneelse_s_ghgi(self):
        # attacker can't save victim ghgi
        r = self.api_attacker.post(
            "/api/save_ghgi/", dict(id=self.ghgi.id), **REQUEST_KWARGS
        )
        self.assertEqual(
            status.HTTP_403_FORBIDDEN,
            r.status_code,
            "A user can't save another user's ghgi",
        )

        # but victim can
        r = self.api_victim.post(
            "/api/save_ghgi/", dict(id=self.ghgi.id), **REQUEST_KWARGS
        )
        # should we raise 404 here ?
        self.assertEqual(
            status.HTTP_201_CREATED, r.status_code, "A user can save her ghgi"
        )

    def test_save_someoneelse_s_vehicules(self):
        # attacker can't save victim ghgi
        r = self.api_attacker.post(
            "/api/save_vehicles/",
            dict(ghgi_id=self.ghgi.id, vehicles=[], synthesis=make_synthesis()),
            **REQUEST_KWARGS,
        )
        self.assertEqual(
            status.HTTP_403_FORBIDDEN,
            r.status_code,
            "A user can't save another user's vehicles",
        )

        # but victim can
        r = self.api_victim.post(
            "/api/save_vehicles/",
            dict(ghgi_id=self.ghgi.id, vehicles=[], synthesis=make_synthesis()),
            **REQUEST_KWARGS,
        )
        # should we raise 404 here ?
        self.assertEqual(
            status.HTTP_201_CREATED, r.status_code, "A user can save her vehicles"
        )

    def test_save_someoneelse_s_vehicules(self):
        # attacker can't save victim ghgi
        r = self.api_attacker.post(
            "/api/save_computer_devices/",
            dict(ghgi_id=self.ghgi.id, devices=[], synthesis=make_synthesis()),
            **REQUEST_KWARGS,
        )
        self.assertEqual(
            status.HTTP_403_FORBIDDEN,
            r.status_code,
            "A user can't save another user's devices",
        )

        # but victim can
        r = self.api_victim.post(
            "/api/save_computer_devices/",
            dict(ghgi_id=self.ghgi.id, devices=[], synthesis=make_synthesis()),
            **REQUEST_KWARGS,
        )
        # should we raise 404 here ?
        self.assertEqual(
            status.HTTP_201_CREATED, r.status_code, "A user can save her devices"
        )

    def test_save_someoneelse_s_travels(self):
        # attacker can't save victim ghgi
        r = self.api_attacker.post(
            "/api/save_travels/",
            dict(ghgi_id=self.ghgi.id, travels=[], synthesis=make_synthesis()),
            **REQUEST_KWARGS,
        )
        self.assertEqual(
            status.HTTP_403_FORBIDDEN,
            r.status_code,
            "A user can't save another user's travels",
        )

        # but victim can
        r = self.api_victim.post(
            "/api/save_travels/",
            dict(ghgi_id=self.ghgi.id, travels=[], synthesis=make_synthesis()),
            **REQUEST_KWARGS,
        )
        # should we raise 404 here ?
        self.assertEqual(
            status.HTTP_201_CREATED, r.status_code, "A user can save her travels"
        )

    def test_save_someoneelse_s_purchases(self):
        # attacker can't save victim ghgi
        r = self.api_attacker.post(
            "/api/save_purchases/",
            dict(ghgi_id=self.ghgi.id, purchases=[], synthesis=make_synthesis()),
            **REQUEST_KWARGS,
        )
        self.assertEqual(
            status.HTTP_403_FORBIDDEN,
            r.status_code,
            "A user can't save another user's purchases",
        )

        # but victim can
        r = self.api_victim.post(
            "/api/save_purchases/",
            dict(ghgi_id=self.ghgi.id, purchases=[], synthesis=make_synthesis()),
            **REQUEST_KWARGS,
        )
        # should we raise 404 here ?
        self.assertEqual(
            status.HTTP_201_CREATED, r.status_code, "A user can save her purchases"
        )

    def test_save_someoneelse_s_survey_message(self):
        # attacker can't save victim ghgi
        r = self.api_attacker.post(
            "/api/save_survey_message/",
            dict(ghgi_id=self.ghgi.id, surveyMessage="test"),
            **REQUEST_KWARGS,
        )
        self.assertEqual(
            status.HTTP_403_FORBIDDEN,
            r.status_code,
            "A user can't save another user's survey message",
        )

        # but victim can
        r = self.api_victim.post(
            "/api/save_survey_message/",
            dict(ghgi_id=self.ghgi.id, surveyMessage="test"),
            **REQUEST_KWARGS,
        )
        # should we raise 404 here ?
        self.assertEqual(
            status.HTTP_201_CREATED, r.status_code, "A user can save her survey message"
        )

    def test_clone_someoneelse_s_survey_message(self):
        # attacker can't save victim ghgi
        r = self.api_attacker.post(
            "/api/clone_survey/",
            dict(ghgi_id=self.ghgi.id, surveyCloneYear="2042"),
            **REQUEST_KWARGS,
        )
        self.assertEqual(
            status.HTTP_403_FORBIDDEN,
            r.status_code,
            "A user can't clone another user's survey",
        )

        # but victim can
        r = self.api_victim.post(
            "/api/clone_survey/",
            dict(ghgi_id=self.ghgi.id, surveyCloneYear="2042"),
            **REQUEST_KWARGS,
        )
        # should we raise 404 here ?
        self.assertEqual(
            status.HTTP_201_CREATED, r.status_code, "A user can clone her survey"
        )




    def test_delete_someoneelse_s_ghgi(self):
        # attacker can't save victim ghgi
        r = self.api_attacker.post(
            "/api/delete_ghgi/", dict(ghgi_id=self.ghgi.id), **REQUEST_KWARGS
        )
        self.assertEqual(
            status.HTTP_403_FORBIDDEN,
            r.status_code,
            "A user can't delete another user's ghgi",
        )

        # but victim can
        r = self.api_victim.post(
            "/api/delete_ghgi/", dict(ghgi_id=self.ghgi.id), **REQUEST_KWARGS
        )
        self.assertEqual(
            status.HTTP_201_CREATED, r.status_code, "A user can delete her ghgi"
        )

    def test_submit_someoneelse_s_ghgi(self):
        # attacker can't save victim ghgi
        r = self.api_attacker.post(
            "/api/submit_data/", dict(ghgi_id=self.ghgi.id, module=""), **REQUEST_KWARGS
        )
        self.assertEqual(
            status.HTTP_403_FORBIDDEN,
            r.status_code,
            "A user can't submit another user's ghgi",
        )

        # but victim can
        r = self.api_victim.post(
            "/api/submit_data/", dict(ghgi_id=self.ghgi.id, module=""), **REQUEST_KWARGS
        )
        # should we raise 404 here ?
        self.assertEqual(status.HTTP_201_CREATED, r.status_code, "A user can her ghgi")

def test_intensity(i: int = 0):
    """util fonction that builds an intensity."""
    return {
        "intensity": i,
        "intensitywc": i + 1,
        "uncertainty": i + 2,
        "uncertaintywc": i + 3,
    }


def make_synthesis(synthesis=None):
    if synthesis is None:
        synthesis = {}
    s = dict(
        electricity=test_intensity(-42),
        heatings=test_intensity(-42),
        refrigerants=test_intensity(-42),
        commutes=test_intensity(-42),
        devices=test_intensity(-42),
        purchases=test_intensity(-42),
        travels=test_intensity(-42),
        vehicles=test_intensity(-42),
    )
    s.update(synthesis)
    return s


class SaveSynthesis(TestCase):
    def setUp(self) -> None:
        # create some resources
        self.user = ensure_user_created()
        self.laboratory = ensure_lab_created(self.user)
        self.ghgi = GHGI(
            laboratory=self.laboratory,
            year=2042,
            nResearcher=1,
            nProfessor=1,
            nEngineer=1,
            nStudent=1,
            budget=int(10e6),
        )
        self.ghgi.save()

        # create a user to interact with the view
        refresh = RefreshToken.for_user(self.user)
        # in django >= 4.2 we could use the headers for that
        self.api_client = Client(
            HTTP_AUTHORIZATION=f"Bearer {refresh.access_token}",
        )

    def generic_save_module(self, module, endpoint):
        """Save a simple synthesis and check its persistence.

        This injects a custom (not valid) synthesis and check it's been
        recorded as it is.
        """
        data = {
            "ghgi_id": self.ghgi.id,
            module: [],
            "synthesis": make_synthesis({module: test_intensity(0)}),
        }

        r = self.api_client.post(endpoint, data, **REQUEST_KWARGS)
        self.assertEqual(status.HTTP_201_CREATED, r.status_code)

        # get the ghgi to check if the synthesis is stored correctly
        ghgi_submitted = GHGI.objects.get(id=self.ghgi.id)

        actual_synthesis = ghgi_submitted.synthesis
        self.assertDictEqual(test_intensity(0), actual_synthesis[module])

    def test_save_travels(self):
        self.generic_save_module("travels", "/api/save_travels/")

    def test_save_commutes(self):
        self.generic_save_module("commutes", "/api/save_commutes/")

    def test_save_vehicles(self):
        self.generic_save_module("vehicles", "/api/save_vehicles/")

    def test_save_computer_devices(self):
        self.generic_save_module("devices", "/api/save_computer_devices/")

    def test_save_purchase(self):
        self.generic_save_module("purchases", "/api/save_purchases/")

    def test_save_buildings(self):
        synthesis = make_synthesis(
            dict(
                heatings=test_intensity(0),
                electricity=test_intensity(1),
                refrigerants=test_intensity(2),
            )
        )
        data = dict(ghgi_id=self.ghgi.id, buildings=[], synthesis=synthesis)

        r = self.api_client.post("/api/save_buildings/", data, **REQUEST_KWARGS)
        self.assertEqual(status.HTTP_201_CREATED, r.status_code)

        # get the ghgi to check if the synthesis is stored correctly
        ghgi_submitted = GHGI.objects.get(id=self.ghgi.id)

        actual_synthesis = ghgi_submitted.synthesis
        self.assertDictEqual(synthesis, actual_synthesis)

    def test_save_activate_survey(self):
        synthesis = make_synthesis()
        synthesis["travels"] = test_intensity(3)

        data = dict(ghgi_id=self.ghgi.id, synthesis=synthesis)
        r = self.api_client.post("/api/activate_ghgi_survey/", data, **REQUEST_KWARGS)
        self.assertEqual(status.HTTP_201_CREATED, r.status_code)

        # get the ghgi to check if the synthesis is stored correctly
        ghgi_submitted = GHGI.objects.get(id=self.ghgi.id)

        actual_synthesis = ghgi_submitted.synthesis
        self.assertDictEqual(synthesis, actual_synthesis)

    def test_save_wrong_format(self):
        synthesis = make_synthesis()
        synthesis["extra_field"] = test_intensity(0)

        data = dict(ghgi_id=self.ghgi.id, buildings=[], synthesis=synthesis)
        r = self.api_client.post("/api/save_buildings/", data, **REQUEST_KWARGS)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, r.status_code)

        data = dict(ghgi_id=self.ghgi.id, commutes=[], synthesis=synthesis)
        r = self.api_client.post("/api/save_commutes/", data, **REQUEST_KWARGS)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, r.status_code)

        data = dict(ghgi_id=self.ghgi.id, devices=[], synthesis=synthesis)
        r = self.api_client.post("/api/save_computer_devices/", data, **REQUEST_KWARGS)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, r.status_code)

        data = dict(ghgi_id=self.ghgi.id, purchases=[], synthesis=synthesis)
        r = self.api_client.post("/api/save_purchases/", data, **REQUEST_KWARGS)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, r.status_code)

        data = dict(ghgi_id=self.ghgi.id, travels=[], synthesis=synthesis)
        r = self.api_client.post("/api/save_travels/", data, **REQUEST_KWARGS)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, r.status_code)

        data = dict(ghgi_id=self.ghgi.id, vehicles=[], synthesis=synthesis)
        r = self.api_client.post("/api/save_vehicles/", data, **REQUEST_KWARGS)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, r.status_code)

        # this also runs on activate survey
        data = dict(ghgi_id=self.ghgi.id, synthesis=synthesis)
        r = self.api_client.post("/api/activate_ghgi_survey/", data, **REQUEST_KWARGS)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, r.status_code)

    def tearDown(self) -> None:
        self.user.delete()
