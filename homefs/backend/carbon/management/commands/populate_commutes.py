import random

from django.core.management.base import BaseCommand, CommandError

from backend.core.models import Laboratory
from backend.users.models import L1P5User
from backend.carbon.models import GHGI, Commute

from backend.test_utils import ensure_lab_created, with_test_user

L1P5_GHGI_YEAR = "2042"
NUMBER_BY_CAT = 2

# make the randomness deterministic
random.seed(42)


@with_test_user
def populate(u, email, password):
    # make it a referent for a laboratory
    l = ensure_lab_created(u)

    # create a ghgi
    g = GHGI(
        laboratory=l,
        year=2042,
        nResearcher=1,
        nProfessor=1,
        nEngineer=1,
        nStudent=1,
        budget=int(10e6),
    )
    g.save()

    km = 20
    nWorkingDay = 1
    for status in ["researcher", "researcher", "engineer", "student"]:
        c = Commute(
            ghgi=g, car=km, engine="diesel", nWorkingDay=nWorkingDay, position=status
        )
        c.save()

    # create a ghgi
    g = GHGI(
        laboratory=l,
        year=2043,
        nResearcher=1,
        nProfessor=1,
        nEngineer=1,
        nStudent=1,
        budget=int(10e6),
    )
    g.save()

    km = 20
    nWorkingDay = 2
    for status in ["researcher", "researcher", "engineer", "student"]:
        c = Commute(
            ghgi=g,
            car=km,
            engine="diesel",
            nWorkingDay=nWorkingDay,
            position=status,
            hasWorkingDay2=True,
            nWorkingDay2=nWorkingDay / 2,
            bus2=10,
        )
        c.save()

    # create a ghgi
    g = GHGI(
        laboratory=l,
        year=2044,
        nResearcher=2,
        nProfessor=2,
        nEngineer=2,
        nStudent=2,
        budget=int(10e6),
    )
    g.save()

    for status in ["researcher", "researcher", "engineer", "student"]:
        c = Commute(
            ghgi=g, car=random.randint(1, 30), engine="diesel", nWorkingDay=random.randint(1, 5), position=status
        )
        c.save()


class Command(BaseCommand):
    help = "Closes the specified poll for voting"

    def handle(self, *args, **options):
        populate()
