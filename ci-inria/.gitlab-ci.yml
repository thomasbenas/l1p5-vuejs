# This pipeline will be launched on INRIA CI:
# https://gitlab.inria.fr/l1p5/l1p5-vuejs/-/pipelines

# Do not trigger the CI if commit message start with '/WIP/'
workflow:
  rules:
    - if: $CI_COMMIT_MESSAGE !~ /^WIP.*/
    - when: always

stages: [test, build, deploy, publish]

variables:
  L1P5APPS: "l1p5apps"

# run locally
#
# gitlab-runner exec docker \
# --cicd-config-file ci-inria/.gitlab-ci.yml \
# --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
# --docker-volumes $(pwd)/output:/output \
# --post-build-script "cp *.tar.gz /output" \
# build
#
#
build:
  image: docker:20.10.16
  stage: build
  tags: [ci.inria.fr, large]
  # services:
  #   - docker:dind
  artifacts:
      paths:
      - $L1P5APPS.tar.gz
  script:
    - env
    - apk add bash git
    - cp conf.example.sh conf.sh
    - DIR=$(pwd)
    - . ./conf.sh
    - echo "Build l1p5 container image $IMAGE"
    - ./build-image.sh
    - docker save $IMAGE | gzip > $L1P5APPS.tar.gz

# Run locally
# it's a bit tricky due to (but allows for debugging the job);
# - artifact management (we need to copy the artifact created by build to the build dir)
# - path management (docker creates siblings container so mount path refers to path on the host system)
#   => we make sure that a path accessed inside a docker refert to the same pass in the runner *and* in the host
#   => we choose to work in $(pwd)/ci-inria/builds and share that directory with
#   the runner (automagically mounted bu the gitlab-runner) and the docker
#   started by the jobs
# 
# gitlab-runner exec docker \
#  --builds-dir $(pwd)/ci-inria/builds \
#  --docker-volumes $(pwd)/ci-inria/builds:$(pwd)/ci-inria/builds \
#  --cicd-config-file ci-inria/.gitlab-ci.yml \
#  --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
#  --custom_build_dir-enabled \
#  --pre-build-script "cp $(pwd)/output/*.tar.gz ." \
#  run-prod

run-prod:
  image: docker:20.10.16
  stage: deploy
  dependencies:
    - build
  # Required for INRIA but useless on gitlab.com
  tags: [ci.inria.fr, large]
  before_script:
    - ls
    - docker load --input $L1P5APPS.tar.gz
  # Required for gitlab.com but useless at INRIA
  # services:
  #  - docker:dind
  script:
    - apk add bash curl git
    - echo "Start l1p5 website in production mode"
    - cp conf.example.sh conf.sh
    - ./start.sh
    - docker volume ls --filter "label=$L1P5APPS"
    - docker ps --filter "label=$L1P5APPS"
    - sleep 5
    - echo "Display processes"
    - docker exec -- l1p5apps ps -ef
    - echo "Wait for website to start"
    - ./wait-start.sh
    - docker exec -- l1p5apps-db ls /run/mysqld
    - docker exec -- l1p5apps-db ps -ef
    - echo "Migrate database"
    - docker exec -t -- $L1P5APPS  python3 manage.py migrate
    - echo "Access l1p5 website from inside the $L1P5APPS container"
    - docker exec -- l1p5apps curl http://localhost:80 | grep "1point5"
    # TODO: investigate, all of this does not work with dind, dind container seems to run on host
    # - echo "Display error logs"
    # - cat /var/log/l1p5-apps/error.log
    # - echo "Display access logs"
    # - cat /var/log/l1p5-apps/access.log
    # - echo "Access l1p5 website on private network"
    # - CONTAINER_IP=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $L1P5APPS) && curl http://$CONTAINER_IP:80
    # - echo "Access l1p5 website on host interface"
    # - curl http://localhost:8182 | grep "1point5"

run-dev:
  image: docker:20.10.16
  stage: deploy
  dependencies:
    - build
  # Required for INRIA but useless on gitlab.com
  tags: [ci.inria.fr, large]
  before_script:
     - docker load --input $L1P5APPS.tar.gz
  # Required for gitlab.com but useless at INRIA
  # services:
  #  - docker:dind
  script:
    - apk add curl bash git
    - echo "Start l1p5 website in development mode"
    - cp conf.example.sh conf.sh
    - ./start.sh -d
    - docker volume ls --filter "label=$L1P5APPS"
    - docker ps --filter "label=$L1P5APPS"
    - sleep 5
    - echo "Display processes"
    - docker exec -- $L1P5APPS ps -ef
    - echo "Wait for website to start"
    - ./wait-start.sh
    - echo "Migrate database"
    - docker exec -t -- $L1P5APPS  python3 manage.py migrate
    - echo "Access l1p5 website from inside the $L1P5APPS container"
    - docker exec -- $L1P5APPS curl http://localhost:80 | grep "1point5"

push:
  image: docker:latest
  stage: publish
  tags: [ci.inria.fr]
  before_script:
    - docker load --input $L1P5APPS.tar.gz
  script:
    - apk add git
    - cp conf.example.sh conf.sh
    - DIR=$(pwd)
    - . ./conf.sh
    - env
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER $CI_REGISTRY --password-stdin
    # If a release tag has been pushed after the commit, it can break CI
    # because GIT_HASH can change during the CI process
    - GIT_HASH="$(git describe --dirty --always)"
    - IMAGE_NAME="$REPO/l1p5apps:$GIT_HASH"
    - docker image tag "$IMAGE" "$IMAGE_NAME"
    - docker image push "$IMAGE_NAME"
