#!/bin/bash

set -euxo pipefail

echo "Install node.js and npm"
apt-get install -y curl software-properties-common
curl -sL https://deb.nodesource.com/setup_14.x | bash -
apt-get -y install nodejs